﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyAppDBConnector;
using Ontology_Module;
using OntoMsg_Module;

namespace ESMaintenance_Module
{
    public partial class frmESMaintenance : Form
    {
        private UserControl_ElasticSearch objUserControl_ElasticSearch;



        private clsLocalConfig objLocalConfig;

        public frmESMaintenance()
        {
            InitializeComponent();
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(Assembly.GetExecutingAssembly().GetType().ToString());
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {

            objUserControl_ElasticSearch = new UserControl_ElasticSearch(objLocalConfig);
            objUserControl_ElasticSearch.Dock = DockStyle.Fill;
            toolStripContainer1.ContentPanel.Controls.Add(objUserControl_ElasticSearch);
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

    }
}
